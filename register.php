<?php
require 'includes/config.php';
include 'includes/header.php';
require 'includes/db.php';
require 'includes/functions.php';

if ($_SERVER ['REQUEST_METHOD'] == 'POST') { //code execution allowed only if this statement is true

    $first = test_input($_POST['firstname']);
    $last = test_input($_POST['lastname']);
    $email =  test_input($_POST['email']);
    $password =  test_input($_POST['password']);
    $confirm_password =  test_input($_POST['confirm']);
    $phone =  test_input($_POST['phone']);
    $user_activationkey = 0;
    $user_token = 0;
    $error = [];

        //Check if any of the required fields is empty
    if (isEmpty([$first, $last, $email, $password, $confirm_password, $phone])) {
        array_push($error,"empty");
    }
        //Check min input size
    if (minLength([[$first,2],[$last,2],[$email,8],[$password,8],[$confirm_password,8],[$phone,8]])) {   
        array_push($error,"min");    
    } 
        //Check max input size
    if (maxLength([[$first,30],[$last,30],[$email,30],[$password,30],[$confirm_password,30],[$phone,15]])) {   
        array_push($error,"max");    
    } 
        //Check if names only contain letters or spaces in between two words
    if (!preg_match("/^[a-zA-Z ]*$/", $first) || !preg_match("/^[a-zA-Z ]*$/", $last) || !preg_match("/^[0-9+]*$/", $phone) || !preg_match("/^[a-zA-Z0-9]*$/", $password)) { 
        array_push($error,"invalid");
    }
        //Check if email is valid
        //Need to add check email address (gmail.com, yahhoo.com etc...)
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        array_push($error,"email");

    } 
        //Check if is there already a user in database with this email
    $sql = "SELECT * FROM users WHERE user_email='$email'";
    $result = mysqli_query($connection, $sql);
    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck>0) {
        array_push($error,"usedmail");
    } 

        //Check if password is rightfully confirmed
    if ($password!=$confirm_password) {
        array_push($error,"password");
    } 
        //If there are some errors, send all messages and inserted values back to signup.php
    if (!empty($error)) {
        $error = http_build_query($error);
        $params = "error=".urlencode($error);
        $params.= "&firstname=".urlencode($first);
        $params.= "&lastname=".urlencode($last);
        $params.= "&email=".urlencode($email);
        $params.= "&phone=".urlencode($phone);

        header("Location: signup.php?".$params);
        exit();
    }
    else {
        //If everything is ok, proceed with registration

            //Password hash
        $mid_password = SALT1.$password.SALT2;
        $final_password = MD5($mid_password);
        
            //Generating random activation key & token
        $user_activationkey = bin2hex(openssl_random_pseudo_bytes(16));
        $user_token = bin2hex(openssl_random_pseudo_bytes(16));
    
            //Insert the user into the database
        $sql = "INSERT INTO users ";
        $sql.="(user_firstname, user_lastname, user_email, user_password, user_phone, user_status, user_activationkey, user_role, user_token ) ";
        $sql.=" VALUES ";
        $sql.=" ('$first','$last','$email','$final_password','$phone','0', '$user_activationkey', '2', '$user_token')";
        $result = mysqli_query($connection, $sql);
    
            //Sending email for confirmation
        $message = "<h1>Confirm Your Registration</h1><br>
        Please, click on this link to confirm your registration.
        http://localhost/projekat/confirm.php?email=$email&code=$user_activationkey";
    
        mail($email,"Confirmation Mail",$message);
        }

}
else {
    header("Location: signup.php"); //automatic redirection to signup.php, if request method is not 'POST'
    exit();
}
?>
<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">
            <p>Dear <strong><?php echo $first." ".$last; ?></strong>,<br>
            Thank you for your registration!<br>
            Please, go to your email account and click on the link in the message we sent you so you can activate your profile!</p>
        </div>
    </div>
</main>
<?php include 'includes/footer.php';