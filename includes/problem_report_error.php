<?php
if(!defined("SECRET")){
    die();
}
?>
<?php
    $error = $car_plate = $car_brand = $email = $car_name = $comment_user = $reservation_day = $reservation_hour = $duration = $problem_service = "";
    $msg = [];
    $ps = [];
    if (isset($_GET['car_plate'])) { $car_plate = $_GET['car_plate']; }
    if (isset($_GET['car_name'])) { $car_name = $_GET['car_name']; }
    if (isset($_GET['comment_user'])) { $comment_user = $_GET['comment_user']; }
    if (isset($_GET['reservation_day'])) { $reservation_day = $_GET['reservation_day']; }

    if (isset($_GET['error'])) { $error = $_GET['error']; }

    //$error is an array turned to string (in register.php) that contains error names of all inputs.
    //First we have to decode it, return as an array(explode) and then show all the errors in $msg.

    $error = urldecode($error);
    $errors = explode("&",$error);

    foreach ($errors as $err) {
        array_push($msg, substr($err, 2));
    }
    if (isset($_GET['problem_service'])) { 
        $problem_service = $_GET['problem_service']; 
        $services = urldecode($problem_service);
        $services = explode("&", $services);
        foreach ($services as $service) {
            array_push($ps, substr($service, 2));
        }
    }    

    if (in_array("empty", $msg)) {
        echo "<p>All fields are required!</p>";
    }
    if (in_array("min", $msg)) {
        echo "<p>Min length: <br>
        Car Plate: 5 characters,
        Car name: 2 characters,
        Comment: 10 characters
        </p>";              
    }
    if (in_array("max", $msg)) {
        echo "<p>Max length: <br>
        Car Plate: 8 characters,
        Car name: 16 characters,
        Comment: 250 characters
        </p>";              
    }
    if (in_array("invalid", $msg)) {
        echo "<p>Please, insert only allowed characters!</p>";
    }

?>