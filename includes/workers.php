<?php
    if(!defined("SECRET")){
        die();
    }
    include_once 'config.php';
    include_once 'db.php';

?>
            <div class="col text-center p-5">
                <h2 class="dislpay-4">Meet Our Team</h2>
            </div>
            <div class="row aboutworker">


<?php

    $sql = "SELECT * FROM workers";
    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $firstname = $row['worker_firstname'];
        $lastname = $row['worker_lastname'];
        $email = $row['worker_email'];
        $phone = $row['worker_phone'];
        $description = $row['worker_about'];
        $photo = substr($row['worker_photo'], 3); ?>

                <div class="col-md-4">
                    <h3><?php echo $firstname." ".$lastname; ?></h3>
                    <img src="<?php echo $photo; ?>" alt="<?php echo $firstname.' '.$lastname; ?>" class="img-fluid mb-3 worker-img">

                    <p><?php echo $description; ?></p>
                    <p><strong>Email: </strong><?php echo $email; ?></p>
                    <p><strong>Phone: </strong><?php echo $phone; ?></p>
                </div>


            <?php
    }
?>
            </div>
