<?php 
include 'includes/config.php';
include 'includes/header.php'; ?>
<?php
if (isset($_POST['submit'])){
    $secretKey ='6LdqJ2UUAAAAAJxYhKP2-j956XluZ_ftYFg9kg5L';
    $responseKey= $_POST['g-recaptcha-response'];
    $userIP = $_SERVER['REMOTE_ADDR'];
    $url="https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
    $response = json_decode($response);

    if($response->success) {
        echo "Verification success!";
    }
    else {
        echo "Verification failed!";
    }
}
       ?>
    <div class="jumbotron">
        <div class = "col-sm-6 mx-auto" style="color:red;">

        <?php include 'includes/signup_error.php'; ?>

        </div>
        <script src="node_modules/jquery-validation/dist/jquery.validate.js"></script>
        <script type="text/javascript" src="js/form_validation.js"></script>

        <div id="formWindow" class="col-sm-6 mx-auto">
            <form action="register.php" method="POST" id="form_register">
            <div class="form-group">
                <label for="firstname">Your Name</label><br>
                <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Firstname" value="<?php echo $firstname; ?>" minlength=2 maxlength=30 required="true"><br>
            </div>
            <div class="form-group">
                <label for="lastname">Your Last Name</label><br>
                <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Lastname" value="<?php echo $lastname; ?>" minlength=2 maxlength=30 required="true"><br>
            </div>
            <div class="form-group">
                <label for="email">E-Mail</label><br>
                <input type="email" class="form-control" name="email" id="email" placeholder="email@mail.com" value="<?php echo $email; ?>"  minlength=8 maxlength=30 required="true"><br>
            </div>
            <div class="form-group">
                <label for="phone">Phone Number</label><br>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="+381-xxx-xxxx" value="<?php echo $phone; ?>"  minlength=8 maxlength=15 required="true"><br>
            </div>
            <div class="form-group">
                <label for="password">Your Password (at least 8 characters)</label><br>
                <input type="password" class="form-control" name="password" id="password" placeholder="password" minlength=8 maxlength=30 required="true"><br>
            </div>
            <div class="form-group">
                <label for="confirm">Confirm Your Password</label><br>
                <input type="password" class="form-control" name="confirm" id="confirm" placeholder="password" minlength=8 maxlength=30 required="true"><br>
            </div>
            <div class="g-recaptcha" data-sitekey="6LdqJ2UUAAAAAHrsfh7AQNmXo9BCvJ7fZ9hALYlO"></div>
                <input type="submit" name="submit" id="submit" class="btn btn-success" value="Register">
            </form>
        </div>
    </div>
    <script src='https://www.google.com/recaptcha/api.js'></script>

<?php include 'includes/footer.php'; ?>