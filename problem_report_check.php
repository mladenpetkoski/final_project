<?php 
    include 'includes/config.php';
    include 'includes/header.php'; 
    include 'includes/db.php';
    include 'includes/functions.php';


if ($_SERVER ['REQUEST_METHOD'] == 'POST') {

    $car_plate = test_input($_POST['car_plate']);
    $car_brand = test_input($_POST['car_brand']);
    $car_name = test_input($_POST['car_name']);
    $comment_user = test_input($_POST['comment_user']);
    $problem_service = $_POST['problem_service'];
    $reservation_day = test_input($_POST['day']);
    $reservation_hour = (float)test_input($_POST['time']);
    $duration = (float)test_input($_POST['duration']);
    $error = [];



    $user_id = $_SESSION['user_id'];

        //Check if any of the required fields is empty
    if (isEmpty([$car_plate, $car_brand, $car_name, $comment_user, $problem_service, $reservation_day, $reservation_hour, $duration])) {
        array_push($error,"empty");
    }
        //Check min input size
    if (minLength([[$car_plate,5],[$car_name,2],[$comment_user,5]])) {   
        array_push($error,"min");    
    } 
        //Check max input size
    if (maxLength([[$car_plate,8],[$car_name,16],[$comment_user,250]])) {   
        array_push($error,"max");    
    } 
        //Check if names only contain letters or spaces in between two words
    if (!preg_match("/^[A-Z0-9]*$/", $car_plate) || !preg_match("/^[a-zA-Z0-9 ]*$/", $car_name) || !preg_match("/[a-zA-Z0-9.-]/", $comment_user)) { 
        array_push($error,"invalid");
    }

        // If there are some errors, send all messages and inserted values back
    if (!empty($error)) {
        $error = http_build_query($error);
        $params = "error=".urlencode($error);
        $params.= "&car_plate=".urlencode($car_plate);
        $params.= "&car_brand=".urlencode($car_brand);
        $params.= "&car_name=".urlencode($car_name);
        $params.= "&comment_user=".urlencode($comment_user);
        $params.= "&reservation_day=".urlencode($reservation_day);
        $params.= "&reservation_hour=".urlencode($reservation_hour);
        $params.= "&duration=".urlencode($duration);
        $problem_service = http_build_query($problem_service);
        $params.= "&problem_service=".urlencode($problem_service);

        header("Location: problem_report.php?".$params);
        exit();
    }
    else {
        //If everything is ok, insert data into the database

        //Checking if this user with these carplates has already used this car service
                
        $sql = "SELECT car_id, car_user FROM cars WHERE car_plate='$car_plate' AND car_user='$user_id'";
        $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));

        //If not, insert into cars
        if(mysqli_num_rows($result)==0) {
            $sql = "INSERT INTO cars 
            (car_user, car_plate, car_brand, car_name)
            VALUES
            ('$user_id','$car_plate','$car_brand','$car_name')";

            $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));
            $car_id = mysqli_insert_id($connection);

        }
        //If yes, use car_id from the database
        else { 
            while($row = mysqli_fetch_assoc($result)){
            $car_id = $row['car_id'];
            }
        }
        //Insert user, car and comment into problems
        $sql = "INSERT INTO problems 
                (problem_user, problem_car, comment_user) 
                VALUES 
                ('$user_id','$car_id','$comment_user')";
        
        $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error());
        $problem_id = mysqli_insert_id($connection);

        //Insert every specific service for this problem into problem_services
        foreach($problem_service as $oneservice) {
            $service_id = $oneservice;

            $sql_select_service = "SELECT * FROM services WHERE service_id='$service_id'"; 
            $result_select_service = mysqli_query($connection, $sql_select_service) or die('1st Query failed: '.mysqli_error());
            while ($row = mysqli_fetch_array($result_select_service, MYSQLI_BOTH)) {
                $servicefinalprice = $row[3];
                $serviceduration = $row[4];
                $sql_insert_into_ps = "INSERT INTO problem_services (problem_id, service_id, service_finalprice, service_finaltime) 
                                    VALUES ('$problem_id','$service_id','$servicefinalprice','$serviceduration')";
            $result_insert_into_ps = mysqli_query($connection, $sql_insert_into_ps) or die('2nd Query failed: '.mysqli_errno());
            }
        }
        //Count totalprice from sum of all checked services
            $sql_countprice = "SELECT SUM(service_finalprice) FROM problem_services WHERE problem_id = $problem_id";
            $result_countprice = mysqli_query($connection, $sql_countprice) or die ('Query 3 failed: '.mysqli_error($connection));

            $row = mysqli_fetch_array($result_countprice);
            $finalprice = $row[0];

        //Count totaltime

            $sql_counttime = "SELECT SUM(service_finaltime) FROM problem_services WHERE problem_id = $problem_id";
            $result_counttime = mysqli_query($connection, $sql_counttime) or die ('Query 4 failed: '.mysqli_error($connection));

            $row = mysqli_fetch_array($result_counttime);
            $finaltime = $row[0];

        //Insert into reservations                
                function inverseDecimal($hour) {
                    if(floor($hour) != $hour) {
                        $hour = explode(".", $hour);
                        $hour =  $hour[0].":30";
                    } 
                    else {
                        $hour = $hour.":00";
                    }
                        return $hour;
                }
                $reservation_hour = inverseDecimal($reservation_hour);
                $sql = "INSERT INTO reservations (reservation_day, reservation_hour, duration) VALUES ('$reservation_day','$reservation_hour','$duration')";
                $result = mysqli_query($connection, $sql) or die('Query 5 failed: '.mysqli_error());

        //Update problems with reservation information and finalprice      
            $reservation_id = mysqli_insert_id($connection);
            $sql = "UPDATE problems SET totalprice='$finalprice', problem_reservation='$reservation_id' WHERE problem_id='$problem_id'";
            $result = mysqli_query($connection, $sql) or die('Query 6 failed: '.mysqli_error());
    }
}
else {
    header("Location: index.php");
    exit();
}
?>
<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">
            <p>Thank you for choosing our service! <br>
                You will get email about details regarding car acceptance.</p>
        </div>
    </div>
</main>

<?php include 'includes/footer.php'; ?>
