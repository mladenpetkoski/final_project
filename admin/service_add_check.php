<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<?php

    if ($_SERVER['REQUEST_METHOD']=="POST") {
        $problem_id = test_input($_POST['problem_id']);
        $service_id = test_input($_POST['service_id']);
        $comment = test_input($_POST['worker_comment']);
        $price = test_input($_POST['price']);
        $duration = test_input($_POST['duration']);
        
        //Add new service that needs to be done for this problem
        $sql = "INSERT INTO problem_services 
        (problem_id, service_id, worker_comment, service_finalprice, service_finaltime) 
        VALUES 
        ('$problem_id','$service_id','$comment','$price','$duration')";
        $result = mysqli_query($connection, $sql) or die('Query 1 failed: '.mysqli_error($connection));

        // Calculate and update new final price with this inserted service
        $sql_count = "SELECT SUM(service_finalprice) FROM problem_services WHERE problem_id = $problem_id";
        $result_count = mysqli_query($connection, $sql_count) or die ('Query 2 failed: '.mysqli_error($connection));

        $row = mysqli_fetch_array($result_count);
        $totalprice = $row[0];

        $sql_price = "UPDATE problems SET totalprice = '$totalprice' WHERE problem_id = '$problem_id' ";
        $result_price = mysqli_query($connection, $sql_price) or die('Query 3 failed: '.mysqli_error($connection));

        header("Location: problems.php#".$problem_id);
        exit();
    }
    ?>
    <?php include '../includes/footer.php'; ?>