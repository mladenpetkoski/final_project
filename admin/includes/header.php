<?php include '../includes/head.php';
if(!defined("ALLOW_HEADER")){
    die();
}

if (!isset($_SESSION['user_role']) || $_SESSION['user_role']!=1) {
    header("Location: ../index.php");
    exit();
}
$tk = $_SESSION['_token'];

?>

            <nav class="navbar navbar-expand-sm navbar-dark bg-dark fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="../index.php">CarService</a>
                    <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse"  aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse text-center" id="navbarCollapse">
                        <ul class="navbar-nav ml-auto">
                            <li class='nav-item'><a class='btn btn-info' href='start.php'>ADMIN</a></li>
                            <li class="nav-item"><a class="nav-link" href='../index.php'>Home</a></li>
                            <li class="nav-item"><a class="nav-link" href='problem_review.php'>Requests</a></li>
                            <li class="nav-item"><a class="nav-link" href='problems.php'>Current</a></li>
                            <li class="nav-item"><a class="nav-link" href='services_update.php'>Services</a></li>
                            <li class="nav-item"><a class="nav-link" href='archive.php'>Archive</a></li>
                            <li class="nav-item"><a class="nav-link" href='workers_update.php'>Workers</a></li>
                            <li class="nav-item"><a class="nav-link" href='schedule.php'>Schedule</a></li>
                            <li class='nav-item'><a class='btn btn-danger' href='../logout.php?logout=<?php echo $tk; ?>'>LOGOUT</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

