<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php';
        include '../includes/functions.php';

?>
<?php
    $problem_id = 0;
    if(isset($_GET['cd'])) { $problem_id = (test_input($_GET['cd'])/71-5)/3;}

    $sql = "SELECT user_firstname, user_lastname, user_email FROM users INNER JOIN problems ON users.user_id = problems.problem_user 
    WHERE problem_id = '$problem_id'";
    $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

    while ($row = mysqli_fetch_assoc($result)) {
    $user_firstname = $row['user_firstname'];
    $user_lastname = $row['user_lastname'];
    $user_email = $row['user_email'];
    $fullname = $user_firstname." ".$user_lastname;
    }

    $sql = "SELECT problems.problem_status, carstatus.carstatus_name, problems.comment_worker, problems.totalprice FROM problems 
            INNER JOIN carstatus ON problems.problem_status = carstatus.carstatus_id WHERE problem_id = '$problem_id'";
    $result = mysqli_query($connection, $sql) OR die('Query failed: '.mysqli_error($connection));

    while ($row = mysqli_fetch_assoc($result)) {
        $car_status = $row['problem_status'];
        $carstatus_name = $row['carstatus_name'];
        $comment_worker = $row['comment_worker'];
        $totalprice = $row['totalprice'];
    }
    //Different generic messages according to carstatus
    $subject = "Car service: ".$carstatus_name;
    $message = "Dear ".$fullname.",
                                    ";
    if($car_status == 4) {
        $message.= "We have a problem with our service.
        
                    we would like to discuss with you about this.

                    Contact us via our website, email or phone.".$comment_worker;
    }
    elseif($car_status == 5) {
        $message.= "We are happy to inform you that your service is finished."
                    .$comment_worker."Total price: ".$totalprice."
                    You can pick your car whenever you want.";
    }
    else {
        $message.= "Your current status is: ".$carstatus_name.".
                    ".$comment_worker;
    }
    $message.="Sincerely, ";

    mail($user_email,$subject,$message);

    header("Location: problems.php#".$problem_id);
?>
