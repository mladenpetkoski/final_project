<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add or remove current workers</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container text-center sm-6 p-4">
        <a href="workers_add.php" class="btn btn-success sm-6">Add New Worker</a>
    </div>
    <div class="container sm-3 tetx-center">
        <table class='table table-striped table-bordered table-hover'>
            <thead>
                <th>No</th>
                <th>Name</th>
                <th class="danger">DELETE</th> 
            </thead>
            <tbody>
    <?php
    $sql = "SELECT worker_id, worker_firstname, worker_lastname FROM workers";
    $result = mysqli_query($connection, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['worker_id'];
        $first = $row['worker_firstname'];
        $last = $row['worker_lastname'];
        ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $first." ".$last; ?></td>
                <td><a  class="btn btn-danger" href='workers_delete.php?id=<?php echo $id; ?>'>Delete</a></td>
            </tr>
    <?php          
    }
    ?>
            </tbody>
        </table>
    </div>
</main>
<?php include '../includes/footer.php'; ?>