<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';

?>
<?php

    $id = 0;
    if(isset($_GET['id'])) { $id = test_input($_GET['id']);}

    $sql = "DELETE FROM services WHERE services.service_id=$id ";
    $result = mysqli_query($connection, $sql) or die('Query failed: '.mysqli_error($connection));

    header("Location: services_update.php");
    exit();
    ?>
