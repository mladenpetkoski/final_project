<?php   
        include '../includes/config.php';
        include 'includes/header.php'; 
        include '../includes/db.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Past Services</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container text-center sm-6 p-4">
        <div class="row text-center">
            <div class="col-sm-6">
                <a href="resolved.php" class="btn btn-success col-sm-6">Resolved Cases</a>
            </div>
            <div class="col-sm-6">
                <a href="declined.php" class="btn btn-danger col-sm-6">Declined Cases</a>
            </div>
        </div>
    </div>
</main>
<?php include '../includes/footer.php'; ?>