<?php  
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
        include '../includes/functions.php';
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Change current and add new services</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container text-center sm-6 p-4">
        <a href="services_add.php" class="btn btn-success sm-6">Add New Service</a>
    </div>

    <div class="container">
        <div class="row">
                <div class="client">
                    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
                    <script>

                $(document).ready(function() {
                    $('#example').DataTable();
                } )

                    </script>

            <table id="example" class="table table-striped table-bordered table-hover">
                <thead>
                    <th>Service Name</th>
                    <th>Service Price</th>
                    <th>Service Duration</th>
                    <th>Service Description</th>
                    <th>UPDATE</th>
                    <th>DELETE</th>
                </thead>
                <tbody>

        <?php
            $sql = "SELECT * FROM services";
            $result = mysqli_query($connection, $sql);

            while ($row = mysqli_fetch_assoc($result)) {
                $sid = $row['service_id'];
                $name = $row['service_name'];
                $description = $row['service_description'];
                $price = $row['service_price(RSD)'];
                $duration = $row['service_duration(hrs)'];
                $photo = $row['service_photo'];
                ?>
                    <tr>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $price; ?> RSD</td>
                        <td><?php echo $duration; ?> hours</td>
                        <td><?php echo $description; ?></td>
                        <td><a class="btn btn-info" href='services_change.php?id=<?php echo $sid; ?>'>Update</a></td>
                        <td><a class="btn btn-danger" id="<?php echo $sid; ?>" href='services_delete.php?id=<?php echo $sid; ?>'>Delete</a></td>
                    </tr>
                    <script>
                        document.getElementById("<?php echo $sid; ?>").addEventListener("click", function(event) {
                            event.preventDefault();
                            var choice = confirm("Are you sure you want to delete this service?");
                            if (choice) {
                                window.location.href = this.getAttribute('href');
                            }
                            else {
                                return false;
                            }
                        });    
                    </script>
            <?php
            }
            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>


<?php include '../includes/footer.php'; ?>