<?php 
        include '../includes/config.php';
        include 'includes/header.php';
        include '../includes/db.php'; 
?>
<section class="admin-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>See scheduled appointments</h2>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container">
        <div class="row">
            <div class="client">
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <thead>
                        <th>Problem</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Day</th>
                        <th>time</th>
                    </thead>
                    <tbody>      
    <?php
    //Show reservations
    $sql = "SELECT problems.*, carstatus.carstatus_name, users.*, cars.*, carbrands.*, reservations.*
            FROM problems 
            INNER JOIN  carstatus ON problems.problem_status = carstatus_id 
            INNER JOIN  users ON problems.problem_user = users.user_id 
            INNER JOIN  cars ON problems.problem_car = cars.car_id
            INNER JOIN  reservations ON problems.problem_reservation = reservations.reservation_id
            INNER JOIN  carbrands ON cars.car_brand = carbrands.brand_id";
    $where = " WHERE problem_status != 7 AND reservations.reservation_day>=CURDATE()";
    $order = " ORDER BY reservations.report_time ASC";
    $sql.=$where;
    $sql.=$order;
    $result = mysqli_query($connection, $sql) or die('Query 1 failed: '.mysqli_error($connection));

    while ($row=mysqli_fetch_array($result, MYSQLI_BOTH)) {

        $problem_id = $row['problem_id'];
        $user_firstname = $row['user_firstname'];
        $user_lastname = $row['user_lastname'];
        $user_phone = $row['user_phone'];
        $user_token = $row['user_token'];

        $reservation_day = $row['reservation_day'];
        $reservation_hour = $row['reservation_hour'];
        $duration = $row['duration'];

        $starttime = substr($reservation_hour, 0, 5);
        $enddatetime = strtotime($reservation_day." ".$reservation_hour)+$duration*60*60;
        $end = date('Y-m-d H:i', $enddatetime);
        $endtime = date('H:i', $enddatetime);

    ?>

    <tr>
        <td><?php echo $problem_id; ?></td>
        <td><?php echo $user_firstname." ".$user_lastname; ?></td>
        <td><?php echo $user_phone; ?></td>
        <td><?php echo $reservation_day; ?></td>
        <td><?php echo $starttime; ?>-<?php echo $endtime; ?></td>
    </tr>

    <?php
    }
    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>



<?php include '../includes/footer.php'; ?>