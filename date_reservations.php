<?php 
    include 'includes/config.php';
    include 'includes/db.php';
    session_start(); 
?>

<?php
    $date = "";
    if(isset($_POST['date'])) {
        $date = $_POST['date'];
        
    $full = array();//creating empty array in which will reserved hours be returned

    $sql = "SELECT * FROM reservations WHERE reservation_day = '$date'";
    $result = mysqli_query($connection, $sql) or die("Query failed: ".mysqli_error($connection));

    if(mysqli_num_rows($result)>0) {

        while($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $hour = $row['reservation_hour'];
            $starttime = substr($hour, 0, 5);
            $duration = $row['duration'];
            $datetime = strtotime($date." ".$hour)+$duration*60*60;
            $end = date('Y-m-d H:i', $datetime);
            $endtime = date('H:i', $datetime);

            $decimalstart = decimal($starttime)*2;
            $decimalend = decimal($endtime)*2;

            //for every row in reservation for selected day, and for every 30 minutes from starthour to endhour, add to array
            for ($i=$decimalstart; $i<$decimalend; $i=$i+1) {
                array_push($full, $i);
            }
        }
    }

    $response = $full;
    $response = json_encode($response,JSON_PRETTY_PRINT);
    print_r($response);
    }
    else {
        die("Sorry, you cannot access this page.<br>
            Return to <a href='index.php'>HomePage</a>");
    }

    function decimal($time) {
        $hm = explode(":", $time);
            return ($hm[0] + ($hm[1]/60)  );
        }
?>