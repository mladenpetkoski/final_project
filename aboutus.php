<?php 
include 'includes/config.php';
include 'includes/header.php'; ?>

  <!-- SHOWCASE SLIDER -->
  <section id="showcase">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item carousel-image-1 active">
          <div class="container">
            <div class="carousel-caption d-sm-block text-right mb-5">
              <h1 class="display-3">Easily Accessible</h1>
              <p class="lead d-none d-sm-inline-block">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, aperiam vel ullam deleniti reiciendis ratione
                quod aliquid inventore vero perspiciatis.</p>
              <a href="index.php#contact" class="btn btn-info btn-md">Contact Us</a>
            </div>
          </div>
        </div>

        <div class="carousel-item carousel-image-2">
          <div class="container">
            <div class="carousel-caption d-sm-block mb-5">
              <h1 class="display-3">Wide Service Offer</h1>
              <p class="lead d-none d-sm-inline-block">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, aperiam vel ullam deleniti reiciendis ratione
                quod aliquid inventore vero perspiciatis.</p>
              <a href="pricelist.php" class="btn btn-primary btn-md">See Our Offer</a>
            </div>
          </div>
        </div>

        <div class="carousel-item carousel-image-3">
          <div class="container">
            <div class="carousel-caption d-sm-block text-right mb-5">
              <h1 class="display-3">Experienced Workers</h1>
              <p class="lead d-none d-sm-inline-block">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, aperiam vel ullam deleniti reiciendis ratione
                quod aliquid inventore vero perspiciatis.</p>
              <a href="index.php#workers" class="btn btn-success btn-md">Our Team</a>
            </div>
          </div>
        </div>
      </div>

      <a href="#myCarousel" data-slide="prev" class="carousel-control-prev">
        <span class="carousel-control-prev-icon"></span>
      </a>

      <a href="#myCarousel" data-slide="next" class="carousel-control-next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
  </section>

  <!-- PHOTO GALLERY -->
  <section id="gallery" class="py-5">
    <div class="container">
      <h1 class="text-center">Photo Gallery</h1>
      <p class="text-center">Check out our photos</p>
      <div class="row mb-4">
        <div class="col-md-4">
          <a href="images/woman-mechanic.jpeg" data-toggle="lightbox" data-gallery="img-gallery" data-height="560"
            data-width="560">
            <img src="images/woman-mechanic.jpeg" width="560" height="560" alt="" class="img-fluid">
          </a>
        </div>

        <div class="col-md-4">
          <a href="images/construction-worker-concrete-hummer-vibrator-38600.jpeg" data-toggle="lightbox" data-gallery="img-gallery" data-height="561"
            data-width="561">
            <img src="images/construction-worker-concrete-hummer-vibrator-38600.jpeg" alt="" class="img-fluid">
          </a>
        </div>

        <div class="col-md-4">
          <a href="images/628110734-1024x1024.jpg" data-toggle="lightbox" data-gallery="img-gallery" data-height="562"
            data-width="562">
            <img src="images/628110734-1024x1024.jpg" alt="" class="img-fluid">
          </a>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-md-4">
          <a href="images/pexels-photo-210627.jpeg" data-toggle="lightbox" data-gallery="img-gallery" data-height="563"
            data-width="563">
            <img src="images/pexels-photo-210627.jpeg" alt="" class="img-fluid">
          </a>
        </div>

        <div class="col-md-4">
          <a href="images/pexels-photo-396159.jpeg" data-toggle="lightbox" data-gallery="img-gallery" data-height="564"
            data-width="564">
            <img src="images/pexels-photo-396159.jpeg" alt="" class="img-fluid">
          </a>
        </div>

        <div class="col-md-4">
          <a href="images/488151494-1024x1024.jpg" data-toggle="lightbox" data-gallery="img-gallery" data-height="565"
            data-width="565">
            <img src="images/488151494-1024x1024.jpg" alt="" class="img-fluid">
          </a>
        </div>
      </div>
    </div>
  </section>

<?php include 'includes/footer.php'; ?>