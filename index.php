<?php
include 'includes/config.php';
include 'includes/header.php'; 
?>
    <!-- main section with login menu -->
    <main id="homesection">
        <div class="home-inner">
            <div class="container startcont">
                <div class="row">
                    <?php
                        include 'includes/login_error.php';
                    ?>                    
                </div>
                <div class="row">
                    <div class="col-lg-8 animated bounceInDown">
                        <h1 class="display-4">Give your car the best it deserves</h1>
                        <div class="d-none d-lg-flex flex-row text-justify">
                            <p class="p-4 align-self-start">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione obcaecati, illum minus quaerat delectus amet facilis iste maxime recusandae in quod, ex beatae officia rem inventore, iusto perspiciatis culpa. Amet?</p>
                            <p class="p-4 align-self-end">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis consequatur, ipsam commodi beatae eos pariatur corporis impedit obcaecati quia praesentium est! Harum officiis rerum enim architecto consequatur voluptatum ea iste!locale_filter_matches</p>
                        </div>
                    </div>
                        <div class="col-sm-12 col-md-6 col-lg-4 text-center order-first order-lg-last animated bounceInUp">
                            <div id="loginmenu">
                            <?php include 'includes/loginmenu.php'; ?>
                            </div>
                    </div>
                </div>
                <div class="row d-none d-md-block">
                    <span class="animated bounce"><img src="images/green-arrow.png"></span>
                </div>  
            </div>
        </div>
    </main>

    <!-- about us -->
    <section id="aboutus">
        <div class="container">
        <div class="row">
            <div class="col text-center p-5">
                <h2 class="display-4">Who we are?</h2>
                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea reiciendis pariatur possimus at eos. Laboriosam sed veniam quaerat assumenda fugiat, inventore quia natus repellendus dolore deleniti pariatur quas necessitatibus Lorem, ipsum dolor sit amet consectetur adipisicing elit. Mollitia iusto laudantium, tenetur delectus omnis, qui sequi eaque numquam quasi voluptatibus ipsum at id accusamus non consequatur saepe adipisci officiis ea?</p>
                <a href="aboutus.php" class="btn btn-outline-secondary">Find Out More</a>  
            </div>
        </div>
        </div>
    </section>

    <!-- services -->
    <section id="services">
        <div class="container">
            <?php include 'includes/services.php'; ?>
        </div>
    </section>

    <!-- workers -->
    <section id="workers">
        <div class="container">
            <?php include 'includes/workers.php'; ?>
        </div>
    </section>

    <!-- contact -->
    <section id="contact" class="bg-light py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Get In Touch</h3>
                    <?php include 'includes/contact.php'; ?>
                </div>
                <div class="col-md-6 text-center">
                    <h3>Our Location</h3>
                    <div id="map"></div>
                    <p><i class="fa fa-home" style="font-size:24px"></i> Stari Zednik, Belgrade Road 987</p>
                    <p><i class="fa fa-fax" style="font-size:24px"></i> +381 64 3628085</p>
                    <p><i class="fa fa-envelope-open" style="font-size:24px"></i> lukamatkovicns@gmail.com</p>
                    <p><i class="fa fa-clock-o" style="font-size:24px"></i> Monday-Saturday: 8AM - 4PM</p>
                </div>
                <script src="js/map.js"></script>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3WkwLk4bSEJMwF2FfOUVrhqztFcHmvoA&callback=initMap"></script>
            </div>
        </div>
    </section>

<?php include 'includes/footer.php'; ?>
    