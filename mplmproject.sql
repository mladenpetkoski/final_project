-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 28, 2018 at 03:56 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mplmproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `carbrands`
--

CREATE TABLE `carbrands` (
  `brand_id` tinyint(4) NOT NULL,
  `brand_name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carbrands`
--

INSERT INTO `carbrands` (`brand_id`, `brand_name`) VALUES
(1, 'BMW'),
(2, 'Audi'),
(3, 'Jeep'),
(4, 'Lamborgini'),
(5, 'Reno'),
(6, 'Fiat'),
(7, 'Honda'),
(8, 'Yugo');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `car_id` tinyint(4) NOT NULL,
  `car_user` tinyint(4) NOT NULL,
  `car_plate` varchar(16) NOT NULL,
  `car_brand` tinyint(4) NOT NULL,
  `car_name` varchar(64) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`car_id`, `car_user`, `car_plate`, `car_brand`, `car_name`) VALUES
(1, 14, 'SU123PO', 5, 'Senic'),
(2, 14, 'TI123TI', 1, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `carstatus`
--

CREATE TABLE `carstatus` (
  `carstatus_id` tinyint(4) NOT NULL,
  `carstatus_name` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carstatus`
--

INSERT INTO `carstatus` (`carstatus_id`, `carstatus_name`) VALUES
(1, 'waiting'),
(2, 'accepted'),
(3, 'processing'),
(4, 'problem'),
(5, 'fixed'),
(6, 'resolved'),
(7, 'declined');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `problem_id` tinyint(4) NOT NULL,
  `problem_user` tinyint(4) NOT NULL,
  `problem_car` tinyint(4) NOT NULL,
  `problem_status` tinyint(4) NOT NULL DEFAULT '1',
  `problem_reservation` tinyint(4) DEFAULT NULL,
  `comment_user` text,
  `comment_worker` text,
  `totalprice` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`problem_id`, `problem_user`, `problem_car`, `problem_status`, `problem_reservation`, `comment_user`, `comment_worker`, `totalprice`) VALUES
(1, 14, 1, 3, 1, 'I need multiple services. Maybe its best if we talk in person.', 'you will get 1000RSD discount', 1477),
(2, 14, 2, 1, 2, 'ooh many things are not working currently', NULL, 72800);

-- --------------------------------------------------------

--
-- Table structure for table `problemstatus`
--

CREATE TABLE `problemstatus` (
  `problemstatus_id` tinyint(4) NOT NULL,
  `problemstatus_name` varchar(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problemstatus`
--

INSERT INTO `problemstatus` (`problemstatus_id`, `problemstatus_name`) VALUES
(1, 'WAITING'),
(2, 'ACCEPTED'),
(3, 'DECLINED');

-- --------------------------------------------------------

--
-- Table structure for table `problem_services`
--

CREATE TABLE `problem_services` (
  `ps_id` int(11) NOT NULL,
  `problem_id` tinyint(4) NOT NULL,
  `service_id` tinyint(4) NOT NULL,
  `worker_comment` text,
  `service_finalprice` int(11) NOT NULL,
  `service_finaltime` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problem_services`
--

INSERT INTO `problem_services` (`ps_id`, `problem_id`, `service_id`, `worker_comment`, `service_finalprice`, `service_finaltime`) VALUES
(1, 1, 1, 'small discount, first customer', 777, 1),
(2, 1, 2, NULL, 1200, 1.5),
(3, 1, 5, '30 minutes', 500, 0.5),
(4, 1, 6, 'sorry, we couldn\'t have fixed this', 0, 3),
(5, 2, 1, NULL, 800, 1),
(6, 2, 3, NULL, 9000, 1.5),
(7, 2, 4, NULL, 50000, 3),
(8, 2, 5, NULL, 1000, 0.5),
(9, 2, 6, NULL, 12000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `reservation_id` tinyint(4) NOT NULL,
  `reservation_day` date DEFAULT NULL,
  `reservation_hour` time DEFAULT NULL,
  `report_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `duration` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`reservation_id`, `reservation_day`, `reservation_hour`, `report_time`, `duration`) VALUES
(1, '2018-07-31', '08:30:00', '2018-07-28 09:37:52', 3.5),
(2, '2018-07-30', '08:30:00', '2018-07-28 09:53:02', 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(1) NOT NULL,
  `role_name` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(64) NOT NULL,
  `service_description` text,
  `service_price(RSD)` int(11) NOT NULL,
  `service_duration(hrs)` float NOT NULL,
  `service_photo` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_description`, `service_price(RSD)`, `service_duration(hrs)`, `service_photo`) VALUES
(1, 'Oil, Lube and Filter Services', 'Our service staff will inspect the oil filter during each oil change, and replace the oil filter in order to keep your oil clean from contaminants. Routine lube and oil changes will keep your engine running stronger and running longer. Contact us online or call us today regarding your next lube, oil, and filter change.', 800, 1, '../images/services/1-oillube.jpg'),
(2, 'Check Engine Light Diagnostics', 'At Precision Tune Auto Care, we have the tools, training and technology to properly handle diagnostic issues for everything from a Check Engine light to an ABS Warning light. Properly performing a complete diagnostic procedure normally includes the following steps:\r\n\r\n', 1200, 1.5, '../images/services/2-lightdiagnostic.jpg'),
(3, 'Cooling System Service &amp; Repair', 'All internal combustion engines waste over 75% of their energy in the production of heat that needs to be removed from the engine while it is operating. To accomplish this, all cars have a cooling system consisting of a pump, radiator, a thermostat, and hoses to route coolant from the hot engine. This is cooled by the air passing through the radiator and returned to the engine to start the process again. If this system fails due to a leak or lack of flow, the vehicle could overheat, damaging other engine components. A complete inspection of this system periodically is essential to prevent costly failure items while you are on the road.\r\n\r\nThis system is so important that our staff does a visual inspection of every vehicle that comes into our shop.', 9000, 1.5, '../images/services/Cool.jpg'),
(4, 'Engine Replacement', 'An automobile engine replacement is an engine or a major part of one that is sold individually without any other parts required to make a functional car (for example a drivetrain). These engines are produced either as aftermarket parts or as reproductions of an engine that has gone out of production.\r\n', 50000, 3, '../images/services/3-replaceengine.jpg'),
(5, 'consultation', 'if you want to talk with us', 1000, 0.5, '../images/services/4-consultation.jpg'),
(6, 'Brake Repair', 'Replacement and installation of brake hardware, rotors, calipers, brake hoses, drums and master brake cylinders.\r\nResurfacing of drums and rotors if necessary\r\nCleaning and adjusting drum brakes and parking brakes\r\nBrake fluid flush and removal of air from the brake lines\r\nAnti-lock Braking System (ABS) diagnostics and repair', 12000, 3, '../images/services/Brake Repair.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `user_firstname` varchar(32) NOT NULL,
  `user_lastname` varchar(32) NOT NULL,
  `user_email` varchar(48) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_phone` varchar(16) NOT NULL,
  `user_status` int(2) DEFAULT NULL,
  `user_activationkey` varchar(50) DEFAULT NULL,
  `user_role` int(2) DEFAULT NULL,
  `user_token` varchar(256) NOT NULL,
  `reset_token` varchar(30) NOT NULL,
  `reset_token_expire` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_email`, `user_password`, `user_phone`, `user_status`, `user_activationkey`, `user_role`, `user_token`, `reset_token`, `reset_token_expire`) VALUES
(1, 'Lucciano', 'Matt', 'lukamatkovicns@gmail.com', 'a2ce2c0fd305ec2286360ebfd2f5376d', '+381643628085', 1, '4fb90f038d3dc1de696214c5efc5b781', 1, '', '', '0000-00-00 00:00:00'),
(2, 'Random', 'Person', 'mail@mail.com', 'ca9f3d496da9355bb14037ee00b33c9d', '+381381381', 1, 'cdaa8fb636f432eb9f8f9633a7b82eb4', 2, '', '', '0000-00-00 00:00:00'),
(3, 'Third', 'User', 'user@mail.com', '6b1b6648fe4db50277d300200b48ddf0', '159', 1, '8aec36748d0e920e86160819c2187b3e', 2, '', '', '0000-00-00 00:00:00'),
(14, 'Adam', 'Gontier', 'lkmtkvc@gmail.com', '8cbce92ede0271fb22e93580aac1ce88', '063369369', 1, '0', 2, 'e3680099a43e1a7a7f28e658b6c0c01e', '', '0000-00-00 00:00:00'),
(25, 'Mladen', 'Car', 'zbunjeni.puding@gmail.com', 'e7b8d4474bddcb4d8f323d724af2798f', '0643158233', 1, '0', 2, '0491369445597284f580f20cbf86633a', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

CREATE TABLE `workers` (
  `worker_id` int(11) NOT NULL,
  `worker_firstname` varchar(32) NOT NULL,
  `worker_lastname` varchar(32) NOT NULL,
  `worker_email` varchar(32) DEFAULT NULL,
  `worker_phone` varchar(16) DEFAULT NULL,
  `worker_about` text,
  `worker_photo` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`worker_id`, `worker_firstname`, `worker_lastname`, `worker_email`, `worker_phone`, `worker_about`, `worker_photo`) VALUES
(4, 'Mladen', 'Petkoski', 'mladen.petkoski@gmail.com', '+381643158233', 'mechanic', '../images/workers/MladenPetkoski.jpg'),
(3, 'Luka', 'Matkovic', 'lukamatkovicns@gmail.com', '+381643628085', 'mechanic', '../images/workers/LukaMatkovic.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carbrands`
--
ALTER TABLE `carbrands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `carstatus`
--
ALTER TABLE `carstatus`
  ADD PRIMARY KEY (`carstatus_id`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`problem_id`);

--
-- Indexes for table `problemstatus`
--
ALTER TABLE `problemstatus`
  ADD PRIMARY KEY (`problemstatus_id`);

--
-- Indexes for table `problem_services`
--
ALTER TABLE `problem_services`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`worker_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carbrands`
--
ALTER TABLE `carbrands`
  MODIFY `brand_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `car_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `carstatus`
--
ALTER TABLE `carstatus`
  MODIFY `carstatus_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `problem_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `problemstatus`
--
ALTER TABLE `problemstatus`
  MODIFY `problemstatus_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `problem_services`
--
ALTER TABLE `problem_services`
  MODIFY `ps_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `reservation_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `workers`
--
ALTER TABLE `workers`
  MODIFY `worker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
