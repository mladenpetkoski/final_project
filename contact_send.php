<?php   
        require 'includes/config.php';
        include 'includes/header.php'; 
        require 'includes/db.php';
        require 'includes/functions.php';

$header = $to = $subject = $message = "";
$error = [];

if ($_SERVER ['REQUEST_METHOD'] == 'POST') { //code execution allowed only if this statement is true
        $email = test_input($_POST['email']);
        $subject = test_input($_POST['subject']);
        $message = test_input($_POST['message']);


        //Check if any of the required fields is empty
        if (isEmpty([$email, $subject, $message])) {
        array_push($error,"empty");
        }
        //Check min input size
        if (minLength([[$email,8],[$subject,4],[$message,4]])) {   
        array_push($error,"width");    
        } 
        //Check max input size
        if (maxLength([[$email,30],[$subject,30],[$message,250]])) {   
                array_push($error,"width");    
        } 
        //Check if email is valid
        //Need to add check email address (gmail.com, yahhoo.com etc...)
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        array_push($error,"email");
        } 

        if (!empty($error)) {
                $error = http_build_query($error);
                $params = "error=".urlencode($error);
                $params.= "&email=".urlencode($email);
                $params.= "&subject=".urlencode($subject);
                $params.= "&message=".urlencode($message);
        
                header("Location: index.php?".$params."#contact");
                exit();
        }
        else {
        //If everything is ok, proceed with email sending
        $header = "From: ".$email;
        $to = "lukamatkovicns@gmail.com";

        mail($to, $subject, $message, $header);
    }
}
    else {
            header("Location: index.php");
            exit();
    }

?>
<main id="welcome">
    <div class="home-inner">
        <div class="container startcont">
            <p>Message sent!</p>
        </div>
    </div>
</main>

<?php include 'includes/footer.php'; ?>
