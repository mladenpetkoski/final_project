<?php 
require 'includes/config.php';
include 'includes/header.php';
require 'includes/db.php';
require 'includes/functions.php';
?>

<body>
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-md-6 col-md-offset-3" align="center">
                <form action="forgot_check.php" method="post">
                <input class="form-control" name="email" id="email" placeholder="Your Email Address"><br>
                <input type="submit" class="btn btn-primary" value="Reset Password">
                <br><br>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>
