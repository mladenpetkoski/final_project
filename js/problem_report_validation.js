$(document).ready(function(){
    $.validator.setDefaults({
        errorClass: 'error',
        highlight: function(element){
            $(element)
            .closest('.form-group')
            .addClass('has-error');
        },
        unhighlight: function(element){
            $(element)
            .closest('.form-group')
            .removeClass('has-error');
        },
        errorPlacement: function(error, element) {
            if (element.prop('type')=='checkbox') {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        }
    })
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var check = false;
            return this.optional(element) || regexp.test(value);
        },
        "Please check your input."
    );
$("#form_problemreport").validate({
        errorClass: "error",
        validClass: "ok",


        rules: {
            car_plate: {
                minlength: 5,
                maxlength: 8,
                required: true,
                regex: /^[A-Z0-9]*$/
            },
            car_brand: {
                required: true
            },
            car_name: {
                minlength: 2,
                maxlength: 16,
                required: true,
                regex: /^[a-zA-Z0-9 ]*$/
            },
            comment_user: {
                required: true,
                minlength: 10,
                maxlength: 250
            },
            problem_service: {
                required: true
            },
            day: {
                required: true
            },
            time: {
                required: true
            },
            duration: {
                required: true
            }
        },
        messages: {
            car_plate: {
                minlength: "At least 5 characters required!",
                maxlength: "Max 8 characters allowed!",
                required: "Please, enter your car plate!",
                regex: "Please, use only capital letters and numbers, no spaces, dashes etc."
            },
            car_brand: {
                required: "Please, select the brand of your car!"
            },
            car_name: {
                minlength: "At least 2 characters required!",
                maxlength: "Max 16 characters allowed!",
                required: "Please, enter your carname!",
                regex: "Please, use only letters and numbers, no spaces, dashes etc."
            },
            comment_user: {
                required: "Please, explain your problem!",
                minlength: "At least 10 characters required!",
                maxlength: "Max 250 characters allowed!",
            },
            'problem_service[]': {
                required: "Check at least one service!"
            },
            day:  {
                required: "Please, select a date and then check free time for an appointment!"
            },
            time: {
                required: "Please, select the time you want to come according to our reservations list!"
            },
            duration: {
                required: "Please, select the duration of your visit!"
            }   
        }
    });

});
