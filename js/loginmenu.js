$(document).ready(function(){

    //Show loginform when click on Login button
    $('#lgbtn').on('click',function (){
        $('#loginform').slideDown(1000);
        $('#loginform').removeClass('d-none');
        $('#login, #signup, #forgot').slideUp(1000);
    });

    //Hide Login and Signup buttons if logged in
    $('#finallogin').on('click', function (){
        $("$loginmenu").addClass('d-none');
    });
    
    //Display Login and Signup buttons if logged out
    $('#logout').on('click', function (){
        $("$loginmenu").removeClass('d-none');
    });
})