var date;
var times = [];

function noSundays(date) {
    return [date.getDay() != 0, ''];
}

$(document).ready(function() {
    $("#datepicker").datepicker({ 
        minDate: 0, 
        maxDate: "+1M +10D",
        dateFormat: "yy-mm-dd",
        beforeShowDay: noSundays,
        onSelect: function() {

            date = this.value;
            var result = [];
            $("#tablr").css("display","block");

            $.ajax({
                url: "date_reservations.php",
                type: "POST",
                data: {'date':date},

                success: function(response) {
                        console.log(response);
                        },

                complete: function(result) {
                    $(".hours").css("background-color","green");
                    result = $.parseJSON(result.responseText);
                    times = result;
                    console.log(times);
                    console.log(result);
                    $.each(result, function( index, value ) {
                        $("#"+value).css("background-color","red");
                    });

                }
            });
        }
    });
});
